/* libs/cutils/load_file.c
**
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License"); 
** you may not use this file except in compliance with the License. 
** You may obtain a copy of the License at 
**
**     http://www.apache.org/licenses/LICENSE-2.0 
**
** Unless required by applicable law or agreed to in writing, software 
** distributed under the License is distributed on an "AS IS" BASIS, 
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
** See the License for the specific language governing permissions and 
** limitations under the License.
*/

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "no-android.h"

void *
load_file(const char *fn, unsigned *_sz)
{
	char *data;
	int sz;
	int fd;

	data = 0;
	fd = open(fn, O_RDONLY);
	if(fd < 0) return 0;

	sz = lseek(fd, 0, SEEK_END);
	if(sz < 0) goto oops;

	if(lseek(fd, 0, SEEK_SET) != 0) goto oops;

	data = (char*) malloc(sz + 1);
	if(data == 0) goto oops;

	if(read(fd, data, sz) != sz) goto oops;
	close(fd);
	data[sz] = 0;

	if(_sz) *_sz = sz;
	return data;

oops:
	close(fd);
	if(data != 0) free(data);
	return 0;
}


static int
property_open(const char *prop_id, int flags)
{
	char filename[512] = "/tmp/";
	int fd;
	strncat(filename, prop_id, sizeof(filename) - 5);
	fd = open(filename, flags, S_IRUSR | S_IWUSR);
	if(fd < 0)
		ALOGE("open('%s', %x) failed: %d\n", filename, flags, fd);
	return fd;
}

int
property_get(const char *prop_id, char *val_out, const void *dunno)
{
	int fd = property_open(prop_id, O_RDONLY);
	int sz;
	char filename[512] = "/tmp/";

	if(fd >= 0) {
		sz = lseek(fd, 0, SEEK_END);
		if(sz < 0)
			goto oops;
		if(sz >= PROPERTY_VALUE_MAX)
			sz = PROPERTY_VALUE_MAX - 1;
		if(lseek(fd, 0, SEEK_SET) != 0)
			goto oops;
		if(read(fd, val_out, sz) != sz)
			goto oops;
		val_out[sz] = 0;
		close(fd);
		return 0;
	}

oops:
	if(fd >= 0)
		close(fd);

	if(strcmp(prop_id, "persist.mtk.wcn.combo.chipid") == 0) {
		strcpy(val_out, "-1");
		return 0;
	}
	return -1;
}

int
property_set(const char *prop_id, const char *val)
{
	int fd = property_open(prop_id, O_WRONLY | O_CREAT);
	int sz, ret = 0;
	if(fd < 0)
		return fd;

	sz = strlen(val);
	if(write(fd, val, sz) != sz) {
		ALOGE("write() failed\n");
		ret = -1;
	}
	close(fd);

	ALOGD("property_set() %s = %s\n", prop_id, val);
	return ret;
}

